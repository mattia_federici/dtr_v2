from torch.utils.data import Dataset
from dtr.base_class.base_class import BaseClass


class BaseDataset(Dataset, BaseClass):
    def __init__(self):
        super(BaseDataset, self).__init__()

    def __len__(self):
        raise NotImplementedError

    def __getitem__(self, item):
        raise NotImplementedError

