import hydra
import torch
import logging
from dtr.ai.training_v2.training.training import DTRTrainer
from dtr.ai.training_v2.model.dtr_net import DtrNet
from dtr.ai.training_v2.dataset.datamod import DTRDatamodule


@hydra.main(config_path="../configuration", config_name="configuration.yaml")
def run_training(params):
    logging.debug(params)
    # region Dataset_params
    iteration = params.dataset.number_of_iteration

    training_class_per_iteration = params.dataset.training.training_class_per_iter
    training_support = params.dataset.training.training_support
    training_query = params.dataset.training.training_query

    validation_class_per_iteration = params.dataset.validation.validation_class_per_iter
    validation_support = params.dataset.validation.validation_support
    validation_query = params.dataset.validation.validation_query
    # endregion
    # region Model_params
    in_channel_model = params.model.in_channel
    hidden_dimension = params.model.hidden_dimension
    output_dimension = params.model.output_dimension
    dropout_probability = params.model.dropout_probability
    learning_rate = params.optimizer.learning_rate
    step_size = params.optimizer.step_size
    # endregion

    # region Trainer
    epochs = params.trainer.epochs
    gpus = params.trainer.gpus

    gpus = gpus if torch.cuda.is_available() else None

    # endregion

    # region Log params
    logging.info(f"Iteration: {iteration}")

    logging.info(f"Training class: {training_class_per_iteration}")
    logging.info(f"Training support: {training_support}")
    logging.info(f"Training query: {training_query}")

    logging.info(f"validation class: {validation_class_per_iteration}")
    logging.info(f"Validation support: {validation_support}")
    logging.info(f"Validation query: {validation_query}")

    logging.info(f"Model in_channel: {in_channel_model}")
    logging.info(f"Model hidden_dimension: {hidden_dimension}")
    logging.info(f"Model dropout probability: {dropout_probability}")

    logging.info(f"Training on: {gpus}")
    logging.info(f"Training for : {epochs} epochs")

    # endregion

    data_module = DTRDatamodule(
        training_class_per_iteration=training_class_per_iteration,
        training_support=training_support,
        training_query=training_query,
        validation_class_per_iteration=validation_class_per_iteration,
        validation_support=validation_support,
        validation_query=validation_query,
        number_of_iteration=iteration
    )

    dtr_model = DtrNet(
        in_channel=in_channel_model,
        hidden_dimension=hidden_dimension,
        output_dimension=output_dimension,
        dropout_probability=dropout_probability,
        learning_rate=learning_rate,
        step_size=step_size,
    )

    trainer = DTRTrainer(
        dtr_model,
        data_module,
        gpu_ids=gpus,
        epochs=epochs,
        resume_from_checkpoint=None,
    )

    trainer.run_training()


if __name__ == "__main__":
    run_training()
