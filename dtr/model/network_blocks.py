from torch.nn import BatchNorm2d, Dropout, Conv2d, Sequential, MaxPool2d, ReLU, Module
from typing import Union


def conv_block(in_ch: int, out_ch: int,
               dropout_p: Union[float, None] = None,
               stride: tuple = (1, 1),
               kernel_size: Union[int, tuple] = 3,
               padding: int = 1,
               act_fun: Union[Module, None] = ReLU,
               pool_type: Union[Module, None] = MaxPool2d,
               pool_size: int = 2,
               batch_normalization: bool = True) -> Module:
    """
    Input:
        in_ch: input channels for the layer
        out_ch: output channels for the layer
        dropout_p: probability to introduce droptout: default None (no dropout)
        stride: stride to apply, default (1,1)
        kernel_size: kernel size to use for filters, default is 3
        act_fun: defaults to nn.ReLU, can be None
        pool_type: pool_type, if None there is no pooling
        pool_size: pool_size, ignored if pool_type is None
    """
    components = [Conv2d(in_ch, out_ch, kernel_size, padding=padding, stride=stride)]
    if batch_normalization:
        components.append(BatchNorm2d(out_ch))
    if act_fun is not None:
        components.append(act_fun())
    if dropout_p is not None:
        components.append(Dropout(p=dropout_p))
    if pool_type is not None:
        components.append(pool_type(pool_size))
    return Sequential(*components)

