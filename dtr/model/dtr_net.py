import pytorch_lightning as pl
from torch.nn import Sequential
from dtr_v2.dtr.model.network_blocks import conv_block
from dtr_v2.dtr.loss import PrototypicalLoss
import torch


class DtrNet(pl.LightningModule):
    def __init__(self,
                 in_channel: int = 1,
                 hidden_dimension: int = 32,
                 output_dimension: int = 2,
                 dropout_probability: float = .2,
                 learning_rate: float = 3e-4,
                 step_size: int = 40):

        super(DtrNet, self).__init__()
        self.features = Sequential(
            conv_block(in_ch=in_channel, out_ch=hidden_dimension, stride=(1, 2), dropout_p=dropout_probability),
            conv_block(in_ch=hidden_dimension, out_ch=hidden_dimension * 2, dropout_p=dropout_probability),
            conv_block(in_ch=hidden_dimension * 2, out_ch=hidden_dimension, stride=(1, 2), dropout_p=dropout_probability),
            conv_block(in_ch=hidden_dimension, out_ch=hidden_dimension // 2, dropout_p=dropout_probability),
            conv_block(in_ch=hidden_dimension // 2, out_ch=hidden_dimension // 4, dropout_p=dropout_probability),
            conv_block(in_ch=hidden_dimension // 4, out_ch=output_dimension,
                       kernel_size=(3, 2), padding=0,
                       act_fun=None, batch_normalization=False, pool_type=None),
        )
        self.loss = PrototypicalLoss(n_support=5)
        self.learning_rate = learning_rate
        self.step_size = step_size

    def forward(self, x):
        n_batch = x.size(0)
        x = self.features(x).view(n_batch, -1)
        assert x.shape[-1] == 2, f"Something went wrong. Expected output shape to be 2, received {x.shape[-1]}"
        return x

    def training_step(self, batch, batch_index):
        results = self(batch)

    def training_step_end(self, training_step_outputs):
        print(training_step_outputs)

    def validation_step(self, batch, batch_index):
        results = self(batch)

    def validation_step_end(self, validation_step_outputs):
        print(validation_step_outputs)

    def configure_optimizers(self):
        optimizer = torch.optim.Adam(params=self.parameters(), lr=self.learning_rate, weight_decay=1e-4)
        scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=self.step_size)
        lr_scheduler = {"scheduler": scheduler,
                        "monitor": "val_loss",
                        "interval": "epoch",
                        "frequency": 1
                        }

        return optimizer, lr_scheduler
