from torch.nn import Module
import torch
from torch.nn.functional import log_softmax
from dtr.utilities.prototypical_utilities import euclidean_dist


class PrototypicalLoss(Module):
    def __init__(self, n_support: int):
        super(PrototypicalLoss, self).__init__()
        self.n_support = n_support

    def forward(self, predicted_coords, target):
        return prototypical_loss(predicted_coords, target, self.n_support)


def prototypical_loss(model_output, true_label, n_support, prototypes=None):
    model_output = model_output.cpu()
    true_label = true_label.cpu().long()

    #classi del ground truth
    classes = torch.unique(true_label, sorted=True)

    #numero di classi presenti nel batch
    n_classes = len(classes)

    # n_query_tot = len(model_output)-n_support*n_classes
    n_query_per_class = true_label.eq(classes[0].item()).sum().item()-n_support

    prototypes_id = torch.stack([torch.where(true_label == x)[0][:n_support] for x in classes])

    queries_id = torch.stack([torch.where(true_label == x)[0][n_support:] for x in classes]).view(-1)

    if prototypes is None:
        prototypes = torch.stack([model_output[prototypes_id[indx]].mean(0) for indx in range(len(classes))])
    else:
        prototypes = prototypes.cpu()

    query_sample = model_output[queries_id]

    dists = euclidean_dist(query_sample, prototypes)

    target_inds = torch.arange(0, n_classes)
    target_inds = target_inds.view(n_classes, 1, 1)
    target_inds = target_inds.expand(n_classes, n_query_per_class, 1).long()

    log_p_y = log_softmax(-dists, dim=1).view(n_classes, n_query_per_class, -1)
    loss_val = -log_p_y.gather(2, target_inds).squeeze().view(-1).mean()
    _, y_hat = log_p_y.max(2)
    acc_val = y_hat.eq(target_inds.squeeze()).float().mean()
    return loss_val, acc_val, prototypes

