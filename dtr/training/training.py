import pytorch_lightning as pl
from typing import Union
from pytorch_lightning.loggers import MLFlowLogger


class DTRTrainer(object):
    def __init__(self,
                 dtr_model: pl.LightningModule,
                 dtr_datamodule: pl.LightningDataModule,
                 gpu_ids: Union[tuple, None] = None,
                 epochs: int = 250,
                 resume_from_checkpoint: str = None,
                 ):

        self.model = dtr_model
        self.datamodule = dtr_datamodule

        acceleration = None if gpu_ids is None else "ddp"

        mlf_logger = MLFlowLogger(
            experiment_name="dtr",
            tracking_uri="http://192.168.3.78:5000",
        )

        self.trainer = pl.Trainer(gpus=gpu_ids,
                                  accelerator=acceleration,
                                  num_sanity_val_steps=1,
                                  min_epochs=5,
                                  max_epochs=epochs,
                                  weights_summary="top",
                                  resume_from_checkpoint=resume_from_checkpoint,
                                  logger=mlf_logger,
                                  )

    def run_training(self):
        self.trainer.fit(self.model, datamodule=self.datamodule)
