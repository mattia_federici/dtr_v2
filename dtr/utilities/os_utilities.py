import os


def ensure_file(path: str) -> bool:
    return os.path.isfile(path)


def ensure_directory(directory: str) -> None:
    if not os.path.isdir(directory):
        os.mkdir(directory)

