import pytorch_lightning as pl
from torch.utils.data import DataLoader
from dtr.dataset.batch_sampler import PrototypicalBatchSampler
import torch


class DTRDatamodule(pl.LightningDataModule):
    def __init__(self,
                 training_class_per_iteration: int = 2,
                 validation_class_per_iteration: int = 2,
                 training_support: int = 3,
                 training_query: int = 5,
                 validation_support: int = 3,
                 validation_query: int = 5,
                 number_of_iteration: int = 100,
                 ):

        super(DTRDatamodule, self).__init__()
        self.number_of_iteration = number_of_iteration
        self.validation_query = validation_query
        self.validation_support = validation_support
        self.validation_samples = validation_query + validation_support
        self.training_query = training_query
        self.training_support = training_support
        self.training_samples = training_query + training_support

        self.training_classes_per_iteration = training_class_per_iteration
        self.validation_classes_per_iteration = validation_class_per_iteration

        self.train_dataset = None
        self.val_dataset = None

    # TODO: creazione del dataset
    def setup(self, stage: str = None):
        self.train_dataset = None
        self.val_dataset = None

    def train_dataloader(self) -> DataLoader:
        labels = self.train_dataset.y
        batch_sampler = PrototypicalBatchSampler(labels=labels,
                                                 classes_per_it=self.training_classes_per_iteration,
                                                 num_samples=self.training_samples,
                                                 iterations=self.number_of_iteration)

        dataloader = torch.utils.data.DataLoader(self.train_dataset, sampler=batch_sampler)
        return dataloader

    def val_dataloader(self) -> DataLoader:
        labels = self.val_dataset.y
        batch_sampler = PrototypicalBatchSampler(labels=labels,
                                                 classes_per_it=self.validation_classes_per_iteration,
                                                 num_samples=self.validation_samples,
                                                 iterations=self.number_of_iteration)

        dataloader = torch.utils.data.DataLoader(self.train_dataset, sampler=batch_sampler)
        return dataloader
