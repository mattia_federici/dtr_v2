from dtr.base_class.base_dataset import BaseDataset
from dtr.utilities.os_utilities import ensure_file
import albumentations as alb
import h5py
from typing import List
import numpy as np


classes_dictionary = {
    "good": 0,
    "bad_small": 1,
    "bad_big": 2,
    "completelybroken": 3,
    "not_rised": 4
}

from_label_to_class = {v: k for k, v in classes_dictionary.items()}


class DtrDatasetFromTxt(BaseDataset):
    def __init__(self,
                 txt_file: str,
                 do_augmentation: bool = True,
                 normalize: bool = False,
                 ):
        """

        Args:
            normalize: flag to normalize images
            txt_file: path to txt file
            phase: phase of dataset
            do_augmentation: flag to do augmentation
        """
        super(DtrDatasetFromTxt, self).__init__()
        if "train" in txt_file:
            self.phase = "train"
        else:
            if "validation" in txt_file:
                self.phase = "validation"
            else:
                raise AssertionError(f"Passed txt that doesn't contains"
                                     f"'train' or 'validation'."
                                     f"Received {txt_file}")

        assert ensure_file(txt_file), f"File {txt_file} not found."
        # do aug only in training
        self.do_aug = do_augmentation if self.phase == "train" else False
        self.txt_file = txt_file
        self.normalize = normalize

        self.augmenter = alb.Compose([
            alb.Normalize(),
        ])

        self.images, self.label = self.__extract_dataset_from_file()

        self.logger.info(f"Training file: {self.txt_file}")
        self.logger.info(f"Dataset images: {len(self.images)}")
        self.logger.info(f"Class expected: {np.unique(self.label)}")
        self.logger.info(f"Using normalizaiont: {self.normalize}")

    def __len__(self):
        return len(self.images)

    def __getitem__(self, item):
        return self.images[item], self.label[item]

    def __extract_dataset_from_file(self) -> (List[np.ndarray], List[int]):
        with open(self.txt_file, "r") as fr:
            # uso splitlines per rimuovere "\n" alla fine
            all_h5 = fr.read().splitlines()
        images = []
        labels = []
        for h5 in all_h5:
            data = h5py.File(h5, "r")
            label = data["class"][()].decode("utf-8")
            # TODO: cambiare con le label ammesse
            # TODO: controllare classi estratte dai training perchè non corrispondono a quelle presenti
            if label in [label]:
                if classes_dictionary.get(label) is None:
                    continue
                images.append(data["X"][:])
                labels.append(classes_dictionary.get(label))

        return images, labels
