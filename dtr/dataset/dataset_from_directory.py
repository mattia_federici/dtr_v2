from dtr.base_class.base_dataset import BaseDataset
from dtr.utilities.os_utilities import ensure_file
import albumentations as alb


classes_dictionary = {
    "good": 0,
    "bad_small": 1,
    "bad_big": 2,
    "completelybroken": 3,
    "not_rised": 4
}

from_label_to_class = {v: k for k, v in classes_dictionary.items()}


class DtrDatasetFromDirectory(BaseDataset):
    def __init__(self,
                 h5_file_path: str,
                 phase: str,
                 do_augmentation: bool = True,
                 normalize: bool = False,
                 ):
        """

        Args:
            normalize: flag to normalize images
            h5_file_path: path to h5 file
            phase: phase of dataset
            do_augmentation: flag to do augmentation
        """
        super(DtrDatasetFromDirectory, self).__init__()
        # check if phase is a type know
        assert phase in ["train", "validation", "test"]
        assert ensure_file(h5_file_path), f"File {h5_file_path} not found."
        # do aug only in training
        self.do_aug = do_augmentation if phase == "train" else False
        self.h5_path = h5_file_path
        self.normalize = normalize

        self.augmenter = alb.Compose([
            alb.Normalize(),
        ])

    def __len__(self):
        return 10

    def __getitem__(self, item):
        return [10, 20, 20]
