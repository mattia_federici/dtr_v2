from distutils.core import setup
from Cython.Build import cythonize
import numpy

setup(
  name='Unwrap',
  ext_modules=cythonize('unwrap.pyx'),
  include_dirs=[numpy.get_include()]
)
