import numpy as np
cimport numpy as np
from libc.math cimport sin,cos,pi

DTYPE = np.float64
ctypedef np.float64_t DTYPE_t

cimport cython
@cython.boundscheck(False) # turn off bounds-checking for entire function
@cython.wraparound(False)  # turn off negative index wrapping for entire function

cdef float radians(float deg):
	return deg/180.*(pi)

def ring_warp(np.ndarray[DTYPE_t, ndim=2] img, int centerx, int centery, int inner_radius, int thickness, np.ndarray[DTYPE_t, ndim=2] out):
	cdef int r, k, h, i, j
	cdef float theta
	dim1=img.shape[0]
	dim2=img.shape[1]
	for i in range(thickness):
		r = inner_radius + i
		for j in range(360):
			theta = radians(j)
			k = int(r * cos(theta))
			k = k + centerx
			h = int(r * sin(theta))
			h = h + centery
			if k < dim1 and h<dim2 :
				out[i,j] = img[k,h]
	return
