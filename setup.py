from setuptools import setup, find_packages
from setuptools.extension import Extension
from Cython.Build import cythonize
import numpy


def get_req():
    with open("requirements.txt", mode="r") as f:
        requirements = f.readlines()
    return requirements


extension = Extension(name="dtr.preprocessing.Unwrapp",
                      sources=["dtr/preprocessing/unwrap.pyx"],
                      include_dirs=[numpy.get_include()]
                      )


setup(name='light_dtr',
      version='0.0.1',
      description='DTR package',
      author='Mattia Federici',
      author_email='mattia.federici@orobix.it',
      # excluding retraining packages because of obfuscation v0
      packages=find_packages(),
      ext_modules=cythonize(extension),
      license='MIT',
      python_requires=">3",
      install_requires=get_req(),
      )
